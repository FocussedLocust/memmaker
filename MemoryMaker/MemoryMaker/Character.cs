﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryMaker
{
    class Character
    {
        string name;
        int id;




        public Character(int a_id, string a_name)
        {
            id = a_id;
            name = a_name;
        }

        public List<Memory> Memories = new List<Memory>();

        public void AddMemory(Memory a_memory)
        {
            Memories.Add(a_memory);
        }

        public override string ToString()
        {
            return name;
        }
    }
}
