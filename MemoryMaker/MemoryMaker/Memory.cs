﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoryMaker
{
    class Memory
    {
        public string name;
        public string body;

        public int level = 0;
        public int type = 0;


        public Memory(string a_name, string a_body)
        {
            name = a_name;
            body = a_body;
        }

        public override string ToString()
        {
            return name;
        }
    }
}
