﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MemoryMaker
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            Character Global = new Character(0, "Global");
            Global.AddMemory(new Memory("Origin", "The First Ever Memory"));
            Characters.Items.Add(Global);

        }

        private void UpdateMemoryList()
        {
            if (Characters.SelectedItem != null)
            {
                Memories.DataSource = null;
                Memories.DataSource = (Characters.SelectedItem as Character).Memories;
                Memories.SelectedIndex = Memories.Items.Count-1;
            }
        }

        private void UpdateEditor()
        {
            if (Memories.SelectedItem != null)
            {
                MemoryName.Text = (Memories.SelectedItem as Memory).name;
                MemoryBody.Text = (Memories.SelectedItem as Memory).body;
                MemLevel.SelectedIndex = (Memories.SelectedItem as Memory).level;
                MemType.SelectedIndex = (Memories.SelectedItem as Memory).type;
            }
        }

        //CHARACTERS SECTION

        private void Characters_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(Characters.SelectedIndex < Characters.Items.Count)
            {
                UpdateMemoryList();
            }
        }

        private void AddCharacter_Click(object sender, EventArgs e)
        {
            Character NewCha = new Character(Characters.Items.Count, ChaName.Text);
            Characters.Items.Add(NewCha);
        }

        private void DelCharacter_Click(object sender, EventArgs e)
        {
            if (Characters.SelectedIndex >= 0)
            { Characters.Items.RemoveAt(Characters.SelectedIndex); };
        }


        //MEMORY SECTION

        private void Memories_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Memories.SelectedIndex < Memories.Items.Count)
            {
                UpdateEditor();
            }
        }

        private void AddMemory_Click(object sender, EventArgs e)
        {
           if(Characters.SelectedIndex >= 0)
            {
                Memory NewMem = new Memory(MemName.Text, "");
         
                (Characters.SelectedItem as Character).Memories.Add(NewMem);
                UpdateMemoryList();
            }
         
        }

        private void DelMemory_Click(object sender, EventArgs e)
        {
            if (Memories.SelectedIndex >= 0)
            {
                (Characters.SelectedItem as Character).Memories.Remove((Memories.SelectedItem as Memory));
                UpdateMemoryList();
            };
        }

        private void SaveMem_Click(object sender, EventArgs e)
        {
            if(Memories.SelectedItem != null)
            {
                (Memories.SelectedItem as Memory).name = MemoryName.Text;
                (Memories.SelectedItem as Memory).body = MemoryBody.Text;
                (Memories.SelectedItem as Memory).level = MemLevel.SelectedIndex;
                (Memories.SelectedItem as Memory).type = MemType.SelectedIndex;
                UpdateMemoryList();
            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ExpMem_Click(object sender, EventArgs e)
        {
            List<string> ExportFile = new List<string>();
            string directory = System.IO.Directory.GetParent(System.IO.Directory.GetParent(Environment.CurrentDirectory).ToString()).ToString() + "\\GameSave.txt";
            //    System.IO.Path.GetDirectoryName(Environment.CurrentDirectory
            //System.IO.StreamWriter file = new System.IO.StreamWriter(directory, true);
            for (int c = 0; c < Characters.Items.Count; c++)
            {

                ExportFile.Add("ID--" + c);
                ExportFile.Add(Characters.Items[c].ToString());
                ExportFile.Add("Mems--");
                for (int m = 0; m < (Characters.Items[c] as Character).Memories.Count; m++)
                {
                    ExportFile.Add("-m- " + (Characters.Items[c] as Character).Memories[m].name); // add new memory name
                    ExportFile.Add("<data> " + (Characters.Items[c] as Character).Memories[m].level.ToString() // add data- level
                        + (Characters.Items[c] as Character).Memories[m].type.ToString());                     // add type
                    ExportFile.Add((Characters.Items[c] as Character).Memories[m].body); // add memory data
                }
                ExportFile.Add("--Mems");


                ExportFile.Add("");
            }
            System.IO.File.WriteAllLines(directory, ExportFile);
        }

        private void LoadFile_Click(object sender, EventArgs e)
        {
            Characters.Items.Clear();
            //Memories.Items.Clear();

            System.IO.Stream MyStream;
            var OFD = new OpenFileDialog();
            if(OFD.ShowDialog() == DialogResult.OK)
            {
                MyStream = OFD.OpenFile();
                System.IO.StreamReader Reader = new System.IO.StreamReader(MyStream);
                string line;
                while ((line = Reader.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                    if(line.Contains("ID--"))
                    {
                        int id = Int32.Parse(line.Replace("ID--", ""));
                        line = Reader.ReadLine(); //Get Character Name
                        Console.WriteLine(line);
                        Character NewCha = new Character(id, line); //Create Character
                        Characters.Items.Add(NewCha); // Add Character Name
                        line = Reader.ReadLine(); // Get Mems
                        int memCount = 0;
                        while ((line = Reader.ReadLine()) != null && (!line.Contains("--Mems"))) //until you hit an empty line or the "--Mems" line
                        {
                            if (line.Contains("-m-")) //if it is a new memory entry
                            {
                                line = line.Replace("-m- ", "");
                                Memory NewMem = new Memory(line, ""); //create memory
                                NewCha.AddMemory(NewMem);
                                line = Reader.ReadLine();
                                line = line.Replace("<data> ", "");
                                NewMem.level = (int)Char.GetNumericValue(line[0]);
                                NewMem.type = (int)Char.GetNumericValue(line[1]);



                                memCount += 1;

                            }
                            else
                            {
                                NewCha.Memories[memCount - 1].body += line;
                            }
                        }

                            line = Reader.ReadLine();
                    }
                   
                    //finish making line reader
                }

                MyStream.Close();
            }
        }
    }
}
