﻿namespace MemoryMaker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Characters = new System.Windows.Forms.ListBox();
            this.Memories = new System.Windows.Forms.ListBox();
            this.AddMemory = new System.Windows.Forms.Button();
            this.DelMemory = new System.Windows.Forms.Button();
            this.DelCharacter = new System.Windows.Forms.Button();
            this.AddCharacter = new System.Windows.Forms.Button();
            this.MemoryBody = new System.Windows.Forms.RichTextBox();
            this.MemoryName = new System.Windows.Forms.RichTextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.ChaName = new System.Windows.Forms.TextBox();
            this.MemName = new System.Windows.Forms.TextBox();
            this.SaveMem = new System.Windows.Forms.Button();
            this.MemCha = new System.Windows.Forms.ListBox();
            this.MemObs = new System.Windows.Forms.ListBox();
            this.ExpMem = new System.Windows.Forms.Button();
            this.LoadFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.MemLevel = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.MemType = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.MemName);
            this.panel1.Controls.Add(this.ChaName);
            this.panel1.Controls.Add(this.DelCharacter);
            this.panel1.Controls.Add(this.AddCharacter);
            this.panel1.Controls.Add(this.DelMemory);
            this.panel1.Controls.Add(this.AddMemory);
            this.panel1.Controls.Add(this.Memories);
            this.panel1.Controls.Add(this.Characters);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 551);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.MemType);
            this.panel2.Controls.Add(this.MemLevel);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.LoadFile);
            this.panel2.Controls.Add(this.ExpMem);
            this.panel2.Controls.Add(this.MemObs);
            this.panel2.Controls.Add(this.MemCha);
            this.panel2.Controls.Add(this.SaveMem);
            this.panel2.Controls.Add(this.comboBox1);
            this.panel2.Controls.Add(this.MemoryName);
            this.panel2.Controls.Add(this.MemoryBody);
            this.panel2.Location = new System.Drawing.Point(218, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(424, 551);
            this.panel2.TabIndex = 1;
            // 
            // Characters
            // 
            this.Characters.FormattingEnabled = true;
            this.Characters.Location = new System.Drawing.Point(3, 3);
            this.Characters.Name = "Characters";
            this.Characters.Size = new System.Drawing.Size(192, 238);
            this.Characters.TabIndex = 0;
            this.Characters.SelectedIndexChanged += new System.EventHandler(this.Characters_SelectedIndexChanged);
            // 
            // Memories
            // 
            this.Memories.FormattingEnabled = true;
            this.Memories.Location = new System.Drawing.Point(3, 271);
            this.Memories.Name = "Memories";
            this.Memories.Size = new System.Drawing.Size(196, 251);
            this.Memories.TabIndex = 1;
            this.Memories.SelectedIndexChanged += new System.EventHandler(this.Memories_SelectedIndexChanged);
            // 
            // AddMemory
            // 
            this.AddMemory.Location = new System.Drawing.Point(147, 527);
            this.AddMemory.Name = "AddMemory";
            this.AddMemory.Size = new System.Drawing.Size(21, 19);
            this.AddMemory.TabIndex = 2;
            this.AddMemory.Text = "+";
            this.AddMemory.UseVisualStyleBackColor = true;
            this.AddMemory.Click += new System.EventHandler(this.AddMemory_Click);
            // 
            // DelMemory
            // 
            this.DelMemory.Location = new System.Drawing.Point(174, 527);
            this.DelMemory.Name = "DelMemory";
            this.DelMemory.Size = new System.Drawing.Size(21, 19);
            this.DelMemory.TabIndex = 3;
            this.DelMemory.Text = "-";
            this.DelMemory.UseVisualStyleBackColor = true;
            this.DelMemory.Click += new System.EventHandler(this.DelMemory_Click);
            // 
            // DelCharacter
            // 
            this.DelCharacter.Location = new System.Drawing.Point(174, 246);
            this.DelCharacter.Name = "DelCharacter";
            this.DelCharacter.Size = new System.Drawing.Size(21, 19);
            this.DelCharacter.TabIndex = 5;
            this.DelCharacter.Text = "-";
            this.DelCharacter.UseVisualStyleBackColor = true;
            this.DelCharacter.Click += new System.EventHandler(this.DelCharacter_Click);
            // 
            // AddCharacter
            // 
            this.AddCharacter.Location = new System.Drawing.Point(147, 246);
            this.AddCharacter.Name = "AddCharacter";
            this.AddCharacter.Size = new System.Drawing.Size(21, 19);
            this.AddCharacter.TabIndex = 4;
            this.AddCharacter.Text = "+";
            this.AddCharacter.UseVisualStyleBackColor = true;
            this.AddCharacter.Click += new System.EventHandler(this.AddCharacter_Click);
            // 
            // MemoryBody
            // 
            this.MemoryBody.Location = new System.Drawing.Point(3, 105);
            this.MemoryBody.Name = "MemoryBody";
            this.MemoryBody.Size = new System.Drawing.Size(416, 223);
            this.MemoryBody.TabIndex = 1;
            this.MemoryBody.Text = "<Enter Memory Body>";
            // 
            // MemoryName
            // 
            this.MemoryName.Location = new System.Drawing.Point(4, 35);
            this.MemoryName.Name = "MemoryName";
            this.MemoryName.Size = new System.Drawing.Size(416, 33);
            this.MemoryName.TabIndex = 2;
            this.MemoryName.Text = "<Enter Memory Name>";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(3, 435);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 3;
            // 
            // ChaName
            // 
            this.ChaName.Location = new System.Drawing.Point(4, 246);
            this.ChaName.Name = "ChaName";
            this.ChaName.Size = new System.Drawing.Size(137, 20);
            this.ChaName.TabIndex = 6;
            this.ChaName.Text = "<Character Name>";
            // 
            // MemName
            // 
            this.MemName.Location = new System.Drawing.Point(3, 526);
            this.MemName.Name = "MemName";
            this.MemName.Size = new System.Drawing.Size(137, 20);
            this.MemName.TabIndex = 7;
            this.MemName.Text = "<Memory Name>";
            // 
            // SaveMem
            // 
            this.SaveMem.BackColor = System.Drawing.Color.CornflowerBlue;
            this.SaveMem.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
            this.SaveMem.FlatAppearance.BorderSize = 5;
            this.SaveMem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.SaveMem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveMem.Location = new System.Drawing.Point(368, 499);
            this.SaveMem.Name = "SaveMem";
            this.SaveMem.Size = new System.Drawing.Size(51, 47);
            this.SaveMem.TabIndex = 4;
            this.SaveMem.Text = "SAVE";
            this.SaveMem.UseVisualStyleBackColor = false;
            this.SaveMem.Click += new System.EventHandler(this.SaveMem_Click);
            // 
            // MemCha
            // 
            this.MemCha.FormattingEnabled = true;
            this.MemCha.Location = new System.Drawing.Point(4, 334);
            this.MemCha.Name = "MemCha";
            this.MemCha.Size = new System.Drawing.Size(120, 95);
            this.MemCha.TabIndex = 5;
            // 
            // MemObs
            // 
            this.MemObs.FormattingEnabled = true;
            this.MemObs.Location = new System.Drawing.Point(154, 334);
            this.MemObs.Name = "MemObs";
            this.MemObs.Size = new System.Drawing.Size(120, 95);
            this.MemObs.TabIndex = 6;
            // 
            // ExpMem
            // 
            this.ExpMem.BackColor = System.Drawing.Color.Green;
            this.ExpMem.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ExpMem.FlatAppearance.BorderSize = 5;
            this.ExpMem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.ExpMem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ExpMem.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExpMem.Location = new System.Drawing.Point(61, 499);
            this.ExpMem.Name = "ExpMem";
            this.ExpMem.Size = new System.Drawing.Size(51, 47);
            this.ExpMem.TabIndex = 7;
            this.ExpMem.Text = "EXPORT";
            this.ExpMem.UseVisualStyleBackColor = false;
            this.ExpMem.Click += new System.EventHandler(this.ExpMem_Click);
            // 
            // LoadFile
            // 
            this.LoadFile.BackColor = System.Drawing.Color.Gold;
            this.LoadFile.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.LoadFile.FlatAppearance.BorderSize = 5;
            this.LoadFile.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Maroon;
            this.LoadFile.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.LoadFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadFile.Location = new System.Drawing.Point(4, 499);
            this.LoadFile.Name = "LoadFile";
            this.LoadFile.Size = new System.Drawing.Size(51, 47);
            this.LoadFile.TabIndex = 8;
            this.LoadFile.Text = "LOAD";
            this.LoadFile.UseVisualStyleBackColor = false;
            this.LoadFile.Click += new System.EventHandler(this.LoadFile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(156, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 19);
            this.label1.TabIndex = 9;
            this.label1.Text = "Memory Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(157, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 19);
            this.label2.TabIndex = 9;
            this.label2.Text = "Memory Body";
            // 
            // MemLevel
            // 
            this.MemLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MemLevel.FormattingEnabled = true;
            this.MemLevel.Items.AddRange(new object[] {
            "<Select Level>",
            "Trivial",
            "Minor",
            "Moderate",
            "Major",
            "Defining"});
            this.MemLevel.Location = new System.Drawing.Point(298, 352);
            this.MemLevel.Name = "MemLevel";
            this.MemLevel.Size = new System.Drawing.Size(121, 21);
            this.MemLevel.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(335, 334);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "Level";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(338, 376);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "Type";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MemType
            // 
            this.MemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MemType.FormattingEnabled = true;
            this.MemType.Items.AddRange(new object[] {
            "<Select Type>",
            "Social",
            "Personal",
            "Cultural",
            "Violent"});
            this.MemType.Location = new System.Drawing.Point(298, 394);
            this.MemType.Name = "MemType";
            this.MemType.Size = new System.Drawing.Size(121, 21);
            this.MemType.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 575);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button AddMemory;
        private System.Windows.Forms.ListBox Memories;
        private System.Windows.Forms.ListBox Characters;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button DelCharacter;
        private System.Windows.Forms.Button AddCharacter;
        private System.Windows.Forms.Button DelMemory;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.RichTextBox MemoryName;
        private System.Windows.Forms.RichTextBox MemoryBody;
        private System.Windows.Forms.TextBox ChaName;
        private System.Windows.Forms.TextBox MemName;
        private System.Windows.Forms.Button SaveMem;
        private System.Windows.Forms.ListBox MemCha;
        private System.Windows.Forms.ListBox MemObs;
        private System.Windows.Forms.Button ExpMem;
        private System.Windows.Forms.Button LoadFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox MemLevel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox MemType;
    }
}

